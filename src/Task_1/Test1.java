package Task_1;

public class Test1 {
    private int firstParam;
    private int secondParam;

    public int getFirstParam() {
        return firstParam;
    }

    public void setFirstParam(int firstParam) {
        this.firstParam = firstParam;
    }

    public int getSecondParam() {
        return secondParam;
    }

    public void setSecondParam(int secondParam) {
        this.secondParam = secondParam;
    }

    public int getSumOfParams() {
        int sum = firstParam + secondParam;
        return sum;
    }

    public int getLargesValue() {
        int largestValue = Math.max(firstParam, secondParam);
        return largestValue;
    }
}

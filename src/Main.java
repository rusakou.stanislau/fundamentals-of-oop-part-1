import Task_1.Test1;
import Task_2.Test2;
import Task_3.Triangle;

public class Main {
    public static void main(String[] args) {
        runTask1();
        runTask2();
        runTaskTriangle();
    }

    private static void runTask1() {
        Test1 testClass = new Test1();
        testClass.setFirstParam(3);
        testClass.setSecondParam(11);

        System.out.println("The 1st param: " + testClass.getFirstParam());
        System.out.println("The 2nd param: " + testClass.getSecondParam());
        System.out.println("Show the sum of params: " + testClass.getSumOfParams());
        System.out.println("Show the larges value: " + testClass.getLargesValue());
    }

    private static void runTask2() {
        Test2 testClass2 = new Test2(31, 10);
        System.out.println("The 1st param: " + testClass2.getFirstParam());
        System.out.println("The 2nd param: " + testClass2.getSecondParam());
    }

    private static void runTaskTriangle() {
        Triangle testTriangle = new Triangle(5,8,5);

        double perimeter = testTriangle.calculatePerimeter();
        System.out.println("Perimeter: " + perimeter);

        double area = testTriangle.calculateArea();
        System.out.println("Area: " + area);
    }
}

package Task_2;

public class Test2 {
    private int firstParam;
    private int secondParam;

    public Test2(int firstParam, int secondParam) {
        this.firstParam = firstParam;
        this.secondParam = secondParam;
    }

    public int getFirstParam() {
        return firstParam;
    }

    public void setFirstParam(int firstParam) {
        this.firstParam = firstParam;
    }

    public int getSecondParam() {
        return secondParam;
    }

    public void setSecondParam(int secondParam) {
        this.secondParam = secondParam;
    }

}
